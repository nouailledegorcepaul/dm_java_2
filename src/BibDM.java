import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

  /**
   * Ajoute deux entiers
   * @param a le premier entier à ajouter
   * @param b le deuxieme entier à ajouter
   * @return la somme des deux entiers
   */
  public static int plus(int a, int b){
      return a+b;
  }


  /**
   * Renvoie la valeur du plus petit élément d'une liste d'entiers
   * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
   * @param liste
   * @return le plus petit élément de liste
   */
  public static Integer min(List<Integer> liste){
    if(!liste.isEmpty()){
      Integer res=liste.get(0);
      for(Integer val : liste){
        if (val<res){
          res=val;
        }
      }
      return res;
    }
      return null;
  }


  /**
   * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
   * @param valeur
   * @param liste
   * @return true si tous les elements de liste sont plus grands que valeur.
   */
  public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    if(!liste.isEmpty()){
      for(T valListe : liste){
        if (valListe.compareTo(valeur)<=0){
          return false;
        }
      }
      return true;
    }
    return false;
  }



  /**
   * Intersection de deux listes données par ordre croissant.
   * @param liste1 une liste triée
   * @param liste2 une liste triée
   * @return une liste triée avec les éléments communs à liste1 et liste2
   */
  public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    List<T> listeRes = new ArrayList<T>();
    for (T elem : liste1){
        if(liste2.contains(elem)){
          listeRes.add(elem);
        }
    }
    return listeRes;
  }


  /**
   * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
   * @param texte une chaine de caractères
   * @return une liste de mots, correspondant aux mots de texte.
   */
   public static List<String> decoupe(String texte) {
         List<String> listMots = new ArrayList<>();
         String mot = null;
         for (int i = 0; i < texte.length(); i++) {
             if (!texte.substring(i, i + 1).equals(" ")) {
                 if (mot == null) {
                     mot = texte.substring(i, i + 1);
                 } else {
                     mot += texte.substring(i, i + 1);
                 }
             } else {
                 if (mot != null) {
                     listMots.add(mot);
                 }
                 mot = null;
             }
         }
         if (mot != null) {
             listMots.add(mot);
         }
         return listMots;
     }


  /**
   * Renvoie le mot le plus présent dans un texte.
   * @param texte une chaine de caractères

   * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
   */

  public static String motMajoritaire(String texte){
    if(texte.length()!=0){
      List<String> listeMot = decoupe(texte);
      List<String> listeRes = new ArrayList<>();
      List<String> listeMotsEtudies = new ArrayList<>();
      int nbOccMax=0;
      for (String mot1 : listeMot){
        if (!listeMotsEtudies.contains(mot1)){
          listeMotsEtudies.add(mot1);
          int nbOcc=0;
          for (String mot2 : listeMot){
            if (mot1.equals(mot2)){
              nbOcc=nbOcc+1;
            }
          }
          if (nbOcc>nbOccMax){
            listeRes.clear();
            listeRes.add(mot1);
            nbOccMax=nbOcc;
          }
          else if (nbOcc==nbOccMax){
            listeRes.add(mot1);
          }
        }
      }
      if (listeRes.size()>1){
        for (String motFinal : listeRes){
          if (plusPetitQueTous( motFinal , listeRes)){
            return motFinal;
          }
        }
      }
    return listeRes.get(0);
    }
    return "";
  }

  /**
   * Permet de tester si une chaine est bien parenthesée
   * @param chaine une chaine de caractères composée de ( et de )
   * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et  est bien parenthèsée.
   */
  public static boolean bienParenthesee(String chaine){
    char c1 = chaine.charAt(0);
    char c2 = chaine.charAt(chaine.length()-1);
    if (chaine.length()>1 && chaine.length()%2==0 && c1==('(') && c2==(')')){
      int cpt=0;
      for (int i=0; i<chaine.length(); i++){
        char c3 = chaine.charAt(i);
        if (cpt>-1){
          if(c3==('(')){
            cpt=cpt+1;
          }
          else if(c3==(')')){
            cpt=cpt-1;
          }
        }
        else{
          return false;
        }
      }
      if (cpt==0){
        return true;
      }
    }
    return false;
  }

  /**
   * Permet de tester si une chaine est bien parenthesée
   * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
   * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
   */

  public static boolean bienParentheseeCrochets(String chaine){
    if (bienParenthesee(chaine)){
        int droiteParenthese = 0;
        int gaucheParenthese = 0;
        int gaucheCrochet = 0;
        int droiteCrochet = 0;
        if (chaine.length() != 0) {
            if (chaine.substring(0, 1).equals(")")
                    || (chaine.substring(chaine.length() - 1, chaine.length()).equals("("))
                    || chaine.substring(0, 1).equals("]")
                    || (chaine.substring(chaine.length() - 1, chaine.length()).equals("["))) {
                return false;
            }
            for (int i = 0; i < chaine.length(); i++) {
                if (i >= 2) {
                    if (chaine.substring(i - 2, i + 1).equals("[(]") || chaine.substring(i - 2, i + 1).equals("[)]")) {
                        return false;
                    }
                }
            }
            for (int i = 0; i < chaine.length(); i++) {
                if (chaine.substring(i, i + 1).equals("(")) {
                    gaucheParenthese += 1;
                }
                if (chaine.substring(i, i + 1).equals(")")) {
                    droiteParenthese += 1;
                }
                if (chaine.substring(i, i + 1).equals("[")) {
                    gaucheCrochet += 1;
                }
                if (chaine.substring(i, i + 1).equals("]")) {
                    droiteCrochet += 1;
                }
            }
            if (gaucheParenthese - droiteParenthese != 0 || gaucheCrochet - droiteCrochet != 0) {
                return false;
            }
        }
        return true;
      }
      return false;
    }

  /**
   * Recherche par dichtomie d'un élément dans une liste triée
   * @param liste, une liste triée d'entiers
   * @param valeur un entier
   * @return true si l'entier appartient à la liste.
   */
  public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
    int debut = 0;
    int fin = liste.size()-1;
    while (debut <= fin) {
       int milieu = (debut + fin)/2;
       if (liste.get(milieu) < valeur){
         debut = milieu + 1;
       }
       else if(liste.get(milieu) > valeur){
         fin = milieu - 1;
            }
            else{
              return true;
            }
    }
    return false;
  }
}
